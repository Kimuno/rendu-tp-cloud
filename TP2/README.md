# TP2 Containerization in-depth

Utilisation des processus liés à docker 


# I. Gestion de conteneurs Docker

* 🌞 Mettre en évidence l'utilisation de chacun des processus liés à Docker
  * `dockerd`, `containerd`, `containerd-shim`
  * analyser qui est le père de qui (en terme de processus, avec leurs PIDs)
  * avec la commande `ps` par exemple

Utilisation des processus liés à docker

```
➜ pstree | grep containerd
        |-containerd-+-2*[containerd-shim-+-sleep]
        |            |                    `-9*[{containerd-shim}]]
        |            `-21*[{containerd}]
➜  Documents pstree | grep docker
        |-dockerd---31*[{dockerd}]
```
```
root      1209     1  0 11:03 ?        00:00:03 /usr/bin/containerd
root      2359     1  0 11:03 ?        00:00:07 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
root      8370  1209  0 11:48 ?        00:00:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/fc4afa04b5df5648586c1145c8b2428130a6284a52b9ec1a583d9dd035cf6ac4 -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-runc
```
On constate que containerd est le père de containerd-shim 

Dockerd est pas le père de containerd  
```
root      2359     1  0 11:03 ?        00:00:07 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
maxim    11543  9824  0 13:02 pts/2    00:00:00 grep --color=auto --exclude-dir=.bzr --exclude-dir=CVS --exclude-dir=.git --exclude-dir=.hg --exclude-dir=.svn dockerd

ps -ef |grep container-shim
maxim    11478  9824  0 13:01 pts/2    00:00:00 grep --color=auto --exclude-dir=.bzr --exclude-dir=CVS --exclude-dir=.git --exclude-dir=.hg --exclude-dir=.svn container-shim
```
---

* 🌞 Utiliser l'API HTTP mise à disposition par `dockerd`
  * utiliser un `curl` (ou autre) pour discuter à travers le socker UNIX
  * la [documentation de l'API est dispo en ligne](https://docs.docker.com/engine/api/v1.40/)
  * récupérer la liste des conteneurs
  * récupérer la liste des images disponibles

Liste des conteneurs 

```
[felix@localhost ~]$ curl --unix-socket /var/run/docker.sock http://localhost/containers/json
[{"Id":"fef95484c3e744663be475c7183a07eabe7265e71f8e4a0d8562a735ea1d9969","Names":["/distracted_galois"],"Image":"alpine","ImageID":"sha256:965ea09ff2ebd2b9eeec88cd822ce156f6674c7e99be082c7efac3c62f3ff652","Command":"sleep 9999","Created":1578397503,"Ports":[],"Labels":{},"State":"running","Status":"Up 19 minutes","HostConfig":{"NetworkMode":"default"},"NetworkSettings":{"Networks":{"bridge":{"IPAMConfig":null,"Links":null,"Aliases":null,"NetworkID":"b739652c5d0f838627193cbd13a843b8a91404f58c29ba23540a0d65c2725a79","EndpointID":"b71ff9e72f983eee28a4567ebc37aa9b3c2ff3c8222203550c96843fa77eea25","Gateway":"172.17.0.1","IPAddress":"172.17.0.2","IPPrefixLen":16,"IPv6Gateway":"","GlobalIPv6Address":"","GlobalIPv6PrefixLen":0,"MacAddress":"02:42:ac:11:00:02","DriverOpts":null}}},"Mounts":[]}]
```

Liste des images 

```
[felix@localhost ~]$ curl --unix-socket /var/run/docker.sock http://localhost/containers/json
[{"Id":"fef95484c3e744663be475c7183a07eabe7265e71f8e4a0d8562a735ea1d9969","Names":["/distracted_galois"],"Image":"alpine","ImageID":"sha256:965ea09ff2ebd2b9eeec88cd822ce156f6674c7e99be082c7efac3c62f3ff652","Command":"sleep 9999","Created":1578397503,"Ports":[],"Labels":{},"State":"running","Status":"Up 19 minutes","HostConfig":{"NetworkMode":"default"},"NetworkSettings":{"Networks":{"bridge":{"IPAMConfig":null,"Links":null,"Aliases":null,"NetworkID":"b739652c5d0f838627193cbd13a843b8a91404f58c29ba23540a0d65c2725a79","EndpointID":"b71ff9e72f983eee28a4567ebc37aa9b3c2ff3c8222203550c96843fa77eea25","Gateway":"172.17.0.1","IPAddress":"172.17.0.2","IPPrefixLen":16,"IPv6Gateway":"","GlobalIPv6Address":"","GlobalIPv6PrefixLen":0,"MacAddress":"02:42:ac:11:00:02","DriverOpts":null}}},"Mounts":[]}]
```

# II. Sandboxing

## 1. Namespaces

### A. Exploration manuelle

Les namespaces sont identifiés par un ID de 10 digits. On peut trouver les namespaces d'un processus donné en utilisant le pseudo-filesystem `/proc`. Plus précisément, on peut les trouver dans le répertoire dédié à ce processus : `/proc/PID`.

```
$ ls -al /proc/<PID>/ns
```

> `/proc` et `/sys` sont des API sur le kernel. Elles sont présentées sous la forme de filesystems pour pouvoir être utilisées simplement avec les outils natifs de GNU/Linux (philosophie "tout est fichier").

🌞 Trouver les namespaces utilisés par votre shell. 

Pour trouver le namespace de mon shell, il faut que je trouve le PID de celui-ci

```
➜  ~ echo $$
18682
```

Pour aller dans le namespace du shell

```
➜  ~ ls -al /proc/18682/ns

dr-x--x--x 2 maxim maxim 0 févr. 23 22:15 .
dr-xr-xr-x 9 maxim maxim 0 févr. 23 22:14 ..
lrwxrwxrwx 1 maxim maxim 0 févr. 23 22:15 cgroup -> 'cgroup:[4026531835]'
lrwxrwxrwx 1 maxim maxim 0 févr. 23 22:15 ipc -> 'ipc:[4026531839]'
lrwxrwxrwx 1 maxim maxim 0 févr. 23 22:15 mnt -> 'mnt:[4026531840]'
lrwxrwxrwx 1 maxim maxim 0 févr. 23 22:15 net -> 'net:[4026532008]'
lrwxrwxrwx 1 maxim maxim 0 févr. 23 22:15 pid -> 'pid:[4026531836]'
lrwxrwxrwx 1 maxim maxim 0 févr. 23 22:24 pid_for_children -> 'pid:[4026531836]'
lrwxrwxrwx 1 maxim maxim 0 févr. 23 22:15 user -> 'user:[4026531837]'
lrwxrwxrwx 1 maxim maxim 0 févr. 23 22:15 uts -> 'uts:[4026531838]'
```

> On peut aussi lister l'ensemble des namespaces qui existent sur la machine avec `lsns`

### B. `unshare`

On peut créer des namespaces avec l'appel système `unshare()`. Il existe une commande `unshare` qui nous permet de l'utiliser directement.  

🌞 Créer un pseudo-conteneur à la main en utilisant `unshare`
* lancer une commande `unshare`
* `unshare` doit exécuter le processus `bash`
* ce processus doit utiliser des namespaces différents de votre hôte :
  * réseau
  * mount
  * PID
  * user
* prouver depuis votre `bash` isolé que ces namespaces sont bien mis en place

Dans un premier temps, nous regardons les processus shell déjà lancé.

Ils ont un PID de 1680 et 1804
```
felix@localhost ~]$ ps -edf | grep bash
felix     1680   800  0 22:14 tty1     00:00:00 -bash
felix     1804  1803  0 22:17 pts/0    00:00:00 -bash
felix     1828  1804  0 22:18 pts/0    00:00:00 grep --color=auto bash
```

On lance la commande unshare afin de créer une nouveau namespace avec des paramètres permettant de ne pas partager l'espace des noms réseaux, PID, de montage et user

On lance la commande avec l'argument R pour utilser le user Root car c'est le seul qui est présent dans ce namespace vu qu'on ne partage pas les user et l'argument f pour permettre l'allocation de mémoire

[felix@localhost ~]$ unshare -fmnpUr

On lance la commande pour voir le nouveau processus bash
```
[root@localhost ~]# ps -edf | grep bash
root      1680   800  0 22:14 tty1     00:00:00 -bash
root      1804  1803  0 22:17 pts/0    00:00:00 -bash
root      1830  1829  0 22:19 pts/0    00:00:00 -bash
root      1851  1830  0 22:19 pts/0    00:00:00 grep --color=auto bash
```
On remarque que le PID est différent comme prévu (PID :1830), on passe également en root comme prévu

On va ensuite comparer les namespace que l'on a crée et celui de notre hôte
```
[root@localhost ~]# ls -al /proc/1830/ns
total 0
dr-x--x--x. 2 root root 0 Feb 23 22:20 .
dr-xr-xr-x. 9 root root 0 Feb 23 22:19 ..
lrwxrwxrwx. 1 root root 0 Feb 23 22:20 cgroup -> 'cgroup:[4026531835]'
lrwxrwxrwx. 1 root root 0 Feb 23 22:20 ipc -> 'ipc:[4026531839]'
lrwxrwxrwx. 1 root root 0 Feb 23 22:20 mnt -> 'mnt:[4026532232]'
lrwxrwxrwx. 1 root root 0 Feb 23 22:20 net -> 'net:[4026532235]'
lrwxrwxrwx. 1 root root 0 Feb 23 22:20 pid -> 'pid:[4026532233]'
lrwxrwxrwx. 1 root root 0 Feb 23 22:20 pid_for_children -> 'pid:[4026532233]'
lrwxrwxrwx. 1 root root 0 Feb 23 22:20 user -> 'user:[4026532231]'
lrwxrwxrwx. 1 root root 0 Feb 23 22:20 uts -> 'uts:[4026531838]'
[root@localhost ~]# exit
logout
[felix@localhost ~]$ ls -al /proc/$$/ns
total 0
dr-x--x--x. 2 felix felix 0 Feb 23 22:21 .
dr-xr-xr-x. 9 felix felix 0 Feb 23 22:17 ..
lrwxrwxrwx. 1 felix felix 0 Feb 23 22:21 cgroup -> 'cgroup:[4026531835]'
lrwxrwxrwx. 1 felix felix 0 Feb 23 22:21 ipc -> 'ipc:[4026531839]'
lrwxrwxrwx. 1 felix felix 0 Feb 23 22:21 mnt -> 'mnt:[4026531840]'
lrwxrwxrwx. 1 felix felix 0 Feb 23 22:21 net -> 'net:[4026531992]'
lrwxrwxrwx. 1 felix felix 0 Feb 23 22:21 pid -> 'pid:[4026531836]'
lrwxrwxrwx. 1 felix felix 0 Feb 23 22:21 pid_for_children -> 'pid:[4026531836]'
lrwxrwxrwx. 1 felix felix 0 Feb 23 22:21 user -> 'user:[4026531837]'
lrwxrwxrwx. 1 felix felix 0 Feb 23 22:21 uts -> 'uts:[4026531838]'
```
On constate que l'id de mnt, net, pid et user sont différents
Notre bash est donc bien isolé et ses namespaces sont bien mis en place


### C. Avec docker

Lancer un conteneur qui tourne en tâche de fond, sur un sleep :
```
$ docker run -d debian sleep 99999
```

🌞 Trouver dans quels namespaces ce conteneur s'exécute.

Je lance le conteneur
```
[felix@localhost ~]$ docker run -d debian sleep 9999
a72d2c98dc97d74da9d2a36a5984c09de950e37b2f61bd8af8a33e80e5bfa562
```
Je récupère le PID de mon container
```
[felix@localhost ~]$ docker inspect a72d2c98dc97d74 | grep Pid
 "Pid": 1895,
 "PidMode": "",
 "PidsLimit": null,

[felix@localhost ~]$ sudo ls -al /proc/1895/ns
[sudo] password for felix: 
total 0
dr-x--x--x. 2 root root 0 Feb 23 22:24 .
dr-xr-xr-x. 9 root root 0 Feb 23 22:24 ..
lrwxrwxrwx. 1 root root 0 Feb 23 22:31 cgroup -> 'cgroup:[4026531835]'
lrwxrwxrwx. 1 root root 0 Feb 23 22:31 ipc -> 'ipc:[4026532235]'
lrwxrwxrwx. 1 root root 0 Feb 23 22:31 mnt -> 'mnt:[4026532233]'
lrwxrwxrwx. 1 root root 0 Feb 23 22:24 net -> 'net:[4026532238]'
lrwxrwxrwx. 1 root root 0 Feb 23 22:31 pid -> 'pid:[4026532236]'
lrwxrwxrwx. 1 root root 0 Feb 23 22:31 pid_for_children -> 'pid:[4026532236]'
lrwxrwxrwx. 1 root root 0 Feb 23 22:31 user -> 'user:[4026531837]'
lrwxrwxrwx. 1 root root 0 Feb 23 22:31 uts -> 'uts:[4026532234]'
```
Voici dans quels namespaces ils s'executent

### D. `nsenter`

Il existe une commande qui permet d'exécuter des processus dans un namespace donné : `nsenter`.  

🌞 Utiliser `nsenter` pour rentrer dans les namespaces de votre conteneur en y exécutant un shell
* prouver que vous êtes isolé en terme de réseau, arborescence de processus, points de montage

```
[felix@localhost ~]$  nsenter --target 1895 --mount --uts --ipc --net --pid /bin/sh
/ # 
```

Vérification en terme de réseau 

Sur le namespace 1895
```
/ # ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
5: eth0@if6: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```
Sur le namespace hôte
```
[felix@localhost ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f6:5e:c8 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 83636sec preferred_lft 83636sec
    inet6 fe80::a00:27ff:fef6:5ec8/64 scope link 
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:d8:e8:71 brd ff:ff:ff:ff:ff:ff
    inet 10.1.2.2/24 brd 10.1.2.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fed8:e871/64 scope link 
       valid_lft forever preferred_lft forever
4: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:0b:bf:cd:ac brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:bff:febf:cdac/64 scope link 
       valid_lft forever preferred_lft forever
6: vethf57e0cc@if5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default 
    link/ether 36:c1:80:86:b8:44 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet6 fe80::34c1:80ff:fe86:b844/64 scope link 
       valid_lft forever preferred_lft forever
```
L'isolation réseau fonctionne est bien place en comparant les deux

Vérification des points de montages, l'isolation fonctionne.



Sur le namespace 1895
```
/ # df -h
Filesystem                Size      Used Available Use% Mounted on
overlay                  17.0G      1.7G     15.2G  10% /
tmpfs                    64.0M         0     64.0M   0% /dev
tmpfs                     1.8G         0      1.8G   0% /sys/fs/cgroup
/dev/mapper/cl-root      17.0G      1.7G     15.2G  10% /etc/resolv.conf
/dev/mapper/cl-root      17.0G      1.7G     15.2G  10% /etc/hostname
/dev/mapper/cl-root      17.0G      1.7G     15.2G  10% /etc/hosts
shm                      64.0M         0     64.0M   0% /dev/shm
tmpfs                     1.8G         0      1.8G   0% /proc/asound
tmpfs                     1.8G         0      1.8G   0% /proc/acpi
tmpfs                    64.0M         0     64.0M   0% /proc/kcore
tmpfs                    64.0M         0     64.0M   0% /proc/keys
tmpfs                    64.0M         0     64.0M   0% /proc/timer_list
tmpfs                    64.0M         0     64.0M   0% /proc/sched_debug
tmpfs                     1.8G         0      1.8G   0% /proc/scsi
tmpfs                     1.8G         0      1.8G   0% /sys/firmware
```
Sur l'hôte 
```
[felix@localhost ~]$ df -h
Filesystem           Size  Used Avail Use% Mounted on
devtmpfs             1.9G     0  1.9G   0% /dev
tmpfs                1.9G     0  1.9G   0% /dev/shm
tmpfs                1.9G  8.6M  1.9G   1% /run
tmpfs                1.9G     0  1.9G   0% /sys/fs/cgroup
/dev/mapper/cl-root   17G  1.8G   16G  11% /
/dev/sda1            976M  126M  783M  14% /boot
tmpfs                379M     0  379M   0% /run/user/1000
```
Les deux namespace ne partagent pas les mêmes points de montages, ils sont donc bien isolés les uns des autres.

Vérification de l'arborescence de processus

Sur le namespace 1895
```
/ # ps -ef
PID   USER     TIME  COMMAND
    1 root      0:00 sleep 9999
   12 root      0:00 /bin/sh
   15 root      0:00 ps -ef
```
Sur l'hôte
```
[felix@localhost ~]$ ps 
  PID TTY          TIME CMD
 1804 pts/0    00:00:00 bash
 2090 pts/0    00:00:00 ps 
```
Les deux namespace ne partage la même arborescence de processus, l'isolation est donc bien fonctionnelle
### E. Et alors, les namespaces User ?

L'implémentation des namespaces de type User dans le noyau a mis plus de temps que les autres namespaces.  
Son fonctionnement est aussi radicalement différent des autres, avec chaque utilisateur d'un nouveau namespace qui est mappé vers un utilisateur du namespace parent, afin de garder une gestion de droit cohérente dans le namespace parent.  

Il est cependant possible de l'utiliser, et de demander à Docker de le mettre à profit.  

🌞 Mettez en place la configuration nécessaire pour que Docker utilise les namespaces de type User.

Nous créons un user nommé dockermap et on crée la variable d'environnement username
 ```
[felix@localhost ~]$ sudo adduser dockermap
[felix@localhost ~]$ username="dockermap"
```
On crée un uid et gid et la range maximale lastuid et lastguid
```
[felix@localhost ~]$ uid=$(id -u "$username")
[felix@localhost ~]$ gid=$(id -g "$username")
[felix@localhost ~]$ lastuid=$(( uid + 65536 ))
[felix@localhost ~]$ lastgid=$(( gid + 65536 ))
```
On créer notre range de guid et uid
```
[felix@localhost ~]$ sudo usermod --add-subuids "$uid"-"$lastuid" "$username"
[sudo] password for felix: 
[felix@localhost ~]$ sudo usermod --add-subgids "$gid"-"$lastgid" "$username"
```
Il faut ensuite renommer userns-remap pour pour utiliser le namespace de type user que l'on a créer
```
[felix@localhost ~]$ vim /etc/docker/daemon.json
{
 "userns-remap": "dockermap"
}
```
On restart pour appliquer les modifications
```
[felix@localhost ~]$ systemctl restart docker
```
On lance un conteneur pour voir si celui_ci est dans le namespace user que l'on a crée
```
[felix@localhost ~]$ docker run -d alpine sleep 9999
3f7760e7b1f14f8f97e8fbb93e7ede72d1d48d5fd421762067970277c36dd93d
```
On cherche le PID du conteneur
```
[felix@localhost ~]$ docker inspect 3f7760e7b1f14f8 | grep Pid
 "Pid": 13711,
 "PidMode": "",
 "PidsLimit": null,
```
On vérifie si le namespace user est bien dockermap 
 ```
[felix@localhost ~]$ sudo ls -al /proc/13711/ns
[sudo] password for felix: 
total 0
dr-x--x--x. 2 dockermap dockermap 0 Feb 24 00:33 .
dr-xr-xr-x. 9 dockermap dockermap 0 Feb 24 00:33 ..
lrwxrwxrwx. 1 dockermap dockermap 0 Feb 24 00:36 cgroup -> 'cgroup:[4026531835]'
lrwxrwxrwx. 1 dockermap dockermap 0 Feb 24 00:36 ipc -> 'ipc:[4026532236]'
lrwxrwxrwx. 1 dockermap dockermap 0 Feb 24 00:36 mnt -> 'mnt:[4026532234]'
lrwxrwxrwx. 1 dockermap dockermap 0 Feb 24 00:33 net -> 'net:[4026532239]'
lrwxrwxrwx. 1 dockermap dockermap 0 Feb 24 00:36 pid -> 'pid:[4026532237]'
lrwxrwxrwx. 1 dockermap dockermap 0 Feb 24 00:36 pid_for_children -> 'pid:[4026532237]'
lrwxrwxrwx. 1 dockermap dockermap 0 Feb 24 00:36 user -> 'user:[4026532233]'
lrwxrwxrwx. 1 dockermap dockermap 0 Feb 24 00:36 uts -> 'uts:[4026532235]'
```
Dockermap est bien le nouveau namespace de type user utilisé par docker

### F. Isolation réseau ? 

Observer les opérations liées au réseau lors de l'exécution d'un conteneur
* 🌞 lancer un conteneur simple
  * je vous conseille une image `debian` histoire d'avoir des commandes comme `ip a` qui n'existent pas dans une image allégée comme `alpine`)
  * ajouter une option pour partager un port (n'importe lequel), pour voir plus d'informations
    * `docker run -d -p 8888:7777 debian sleep 99999`
* 🌞 vérifier le réseau du conteneur
  * vérifier que le conteneur a bien une carte réseau et repérer son IP
    * c'est une des interfaces de la *veth pair*
  * possible avec un shell dans le conteneur OU avec un `docker inspect` depuis l'hôte
* 🌞 vérifier le réseau sur l'hôte
  * vérifier qu'il existe une première carte réseau qui porte une IP dans le même réseau que le conteneur
  * vérifier qu'il existe une deuxième carte réseau, qui est la deuxième interface de la *veth pair*
    * son nom ressemble à *vethXXXXX@ifXX*
  * identifier les règles *iptables* liées à la création de votre conteneur  

Lancement du conteneur avec le partage de port
```
[felix@localhost ~]$ sudo docker run -d -p 8888:7777 debian sleep 99999
2c7894168213ff6ccec820a280763b4286ffa3f55bf047c53f7c3cfbd05e2489
```
On ouvre un shell sur ce conteneur 
```
[felix@localhost ~]$ sudo docker exec -it 2c7894168213 bash
```
On vérifie le réseau du conteneur
```
root@2c7894168213:/# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
7: eth0@if8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:03 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.3/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```
La carte réseau du conteneur : eth0@if8 avec pour ip 172.17.0.3/16

On repart sur l'hôte et on vérifie le réseau
```
[felix@localhost ~]$ ip a
4: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:0b:bf:cd:ac brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:bff:febf:cdac/64 scope link 
       valid_lft forever preferred_lft forever
```

Nous retrouvons la carte réseau docker0 qu est sur le même réseau que la carte du conteneur (172.17.0.1/16)
```
8: vethcfbe451@if7: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default 
    link/ether 1a:d2:a1:4b:16:5c brd ff:ff:ff:ff:ff:ff link-netnsid 1
    inet6 fe80::18d2:a1ff:fe4b:165c/64 scope link 
       valid_lft forever preferred_lft forever
```
Nous trouvons ici la carte vethcfbe451@if7, la deuxième interface de la *veth pair*

Voici les règles iptables établies
```
[felix@localhost ~]$ sudo iptables -vnL
Chain INPUT (policy ACCEPT 26283 packets, 70M bytes)
 pkts bytes target     prot opt in     out     source               destination         

Chain FORWARD (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 DOCKER-USER  all  --  *      *       0.0.0.0/0            0.0.0.0/0           
    0     0 DOCKER-ISOLATION-STAGE-1  all  --  *      *       0.0.0.0/0            0.0.0.0/0           
    0     0 ACCEPT     all  --  *      docker0  0.0.0.0/0            0.0.0.0/0            ctstate RELATED,ESTABLISHED
    0     0 DOCKER     all  --  *      docker0  0.0.0.0/0            0.0.0.0/0           
    0     0 ACCEPT     all  --  docker0 !docker0  0.0.0.0/0            0.0.0.0/0           
    0     0 ACCEPT     all  --  docker0 docker0  0.0.0.0/0            0.0.0.0/0           

Chain OUTPUT (policy ACCEPT 9330 packets, 561K bytes)
 pkts bytes target     prot opt in     out     source               destination         

Chain DOCKER (1 references)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 ACCEPT     tcp  --  !docker0 docker0  0.0.0.0/0            172.17.0.3           tcp dpt:7777

Chain DOCKER-ISOLATION-STAGE-1 (1 references)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 DOCKER-ISOLATION-STAGE-2  all  --  docker0 !docker0  0.0.0.0/0            0.0.0.0/0           
    0     0 RETURN     all  --  *      *       0.0.0.0/0            0.0.0.0/0           

Chain DOCKER-ISOLATION-STAGE-2 (1 references)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 DROP       all  --  *      docker0  0.0.0.0/0            0.0.0.0/0           
    0     0 RETURN     all  --  *      *       0.0.0.0/0            0.0.0.0/0           

Chain DOCKER-USER (1 references)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 RETURN     all  --  *      *       0.0.0.0/0            0.0.0.0/0           
```
On constate qu'il y a des règles iptables créee par docker à l'intention de notre conteneur (les CHAIN Docker) qui lui permettent de communiquer avec l'hôte

## 2. Cgroups

### A. Découverte manuelle

🌞 Lancer un conteneur Docker et déduire dans quel cgroup il s'exécute
```

Lancement du conteneur
maxim@maxim-Predator-PH317-52:~$ docker run -d debian sleep 9999 
fc4afa04b5df5648586c1145c8b2428130a6284a52b9ec1a583d9dd035cf6ac4
```
On se regarde dans le répertoire cd .. /sys/fs/cgroup/memory/docker

```
maxim@maxim-Predator-PH317-52:~$ ls -al /sys/fs/cgroup/memory/docker
total 0
drwxr-xr-x 3 root root 0 févr. 24 11:48 <font color="#5555FF"><b>.</b></font>
dr-xr-xr-x 5 root root 0 févr. 24 11:03 <font color="#5555FF"><b>..</b></font>
-rw-r--r-- 1 root root 0 févr. 24 11:54 cgroup.clone_children
--w--w--w- 1 root root 0 févr. 24 11:54 cgroup.event_control
-rw-r--r-- 1 root root 0 févr. 24 11:54 cgroup.procs
drwxr-xr-x 2 root root 0 févr. 24 11:48 fc4afa04b5df5648586c1145c8b2428130a6284a52b9ec1a583d9dd035cf6ac4
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.failcnt
--w------- 1 root root 0 févr. 24 11:54 memory.force_empty
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.kmem.failcnt
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.kmem.limit_in_bytes
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.kmem.max_usage_in_bytes
-r--r--r-- 1 root root 0 févr. 24 11:54 memory.kmem.slabinfo
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.kmem.tcp.failcnt
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.kmem.tcp.limit_in_bytes
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.kmem.tcp.max_usage_in_bytes
-r--r--r-- 1 root root 0 févr. 24 11:54 memory.kmem.tcp.usage_in_bytes
-r--r--r-- 1 root root 0 févr. 24 11:54 memory.kmem.usage_in_bytes
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.limit_in_bytes
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.max_usage_in_bytes
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.move_charge_at_immigrate
-r--r--r-- 1 root root 0 févr. 24 11:54 memory.numa_stat
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.oom_control
---------- 1 root root 0 févr. 24 11:54 memory.pressure_level
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.soft_limit_in_bytes
-r--r--r-- 1 root root 0 févr. 24 11:54 memory.stat
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.swappiness
-r--r--r-- 1 root root 0 févr. 24 11:54 memory.usage_in_bytes
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.use_hierarchy
-rw-r--r-- 1 root root 0 févr. 24 11:54 notify_on_release
-rw-r--r-- 1 root root 0 févr. 24 11:54 tasks

```
Il s'execute dans le Cgroup Docker



* le répertoire `/sys/fs/cgroup` représente la structure arborescente des cgroups
* ainsi le réperoire `/sys/fs/cgroup/memory` contient les informations relatives aux cgroups concernant la RAM
* on peut trouver un sous-répertoire `/sys/fs/cgroup/memory/docker` qui contient des informations relatives aux cgroups liés à Docker

### B. Utilisation par Docker

🌞 Lancer un conteneur Docker et trouver 
* la mémoire RAM max qui lui est autorisée
* le nombre de processus qu'il peut contenir
* explorer un peu de vous-même ce qu'il est possible de faire avec des cgroups  


On constate que le container lancé précedemment se situe dans ce répertoire

Nous allons donc nous rendre dans ce dossier
```
 ls -al /sys/fs/cgroup/memory/docker/fc4afa04b5df5648586c1145c8b2428130a6284a52b9ec1a583d9dd035cf6ac4
total 0
drwxr-xr-x 2 root root 0 févr. 24 11:48 .
drwxr-xr-x 3 root root 0 févr. 24 11:48 ..
-rw-r--r-- 1 root root 0 févr. 24 11:54 cgroup.clone_children
--w--w--w- 1 root root 0 févr. 24 11:48 cgroup.event_control
-rw-r--r-- 1 root root 0 févr. 24 11:48 cgroup.procs
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.failcnt
--w------- 1 root root 0 févr. 24 11:54 memory.force_empty
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.kmem.failcnt
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.kmem.limit_in_bytes
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.kmem.max_usage_in_bytes
-r--r--r-- 1 root root 0 févr. 24 11:54 memory.kmem.slabinfo
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.kmem.tcp.failcnt
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.kmem.tcp.limit_in_bytes
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.kmem.tcp.max_usage_in_bytes
-r--r--r-- 1 root root 0 févr. 24 11:54 memory.kmem.tcp.usage_in_bytes
-r--r--r-- 1 root root 0 févr. 24 11:54 memory.kmem.usage_in_bytes
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.limit_in_bytes
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.max_usage_in_bytes
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.move_charge_at_immigrate
-r--r--r-- 1 root root 0 févr. 24 11:54 memory.numa_stat
-rw-r--r-- 1 root root 0 févr. 24 11:48 memory.oom_control
---------- 1 root root 0 févr. 24 11:54 memory.pressure_level
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.soft_limit_in_bytes
-r--r--r-- 1 root root 0 févr. 24 11:54 memory.stat
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.swappiness
-r--r--r-- 1 root root 0 févr. 24 11:54 memory.usage_in_bytes
-rw-r--r-- 1 root root 0 févr. 24 11:54 memory.use_hierarchy
-rw-r--r-- 1 root root 0 févr. 24 11:54 notify_on_release
-rw-r--r-- 1 root root 0 févr. 24 11:54 tasks
```

Le fichier qui indique la mémoire RAM max qui lui est autorisée est memory.max_usage_in_bytes 
```
cat memory.max_usage_in_bytes 
2564096
```

Pour trouver le maximum de processus que peux contenir le conteneur, il faut revenir dans le fichier cgroup

```
ls -al /sys/fs/cgroup/pids/docker/fc4afa04b5df5648586c1145c8b2428130a6284a52b9ec1a583d9dd035cf6ac4/
total 0
drwxr-xr-x 2 root root 0 févr. 24 11:48 .
drwxr-xr-x 3 root root 0 févr. 24 11:48 ..
-rw-r--r-- 1 root root 0 févr. 24 12:12 cgroup.clone_children
-rw-r--r-- 1 root root 0 févr. 24 11:48 cgroup.procs
-rw-r--r-- 1 root root 0 févr. 24 12:12 notify_on_release
-r--r--r-- 1 root root 0 févr. 24 12:12 pids.current
-r--r--r-- 1 root root 0 févr. 24 12:12 pids.events
-rw-r--r-- 1 root root 0 févr. 24 12:12 pids.max
-rw-r--r-- 1 root root 0 févr. 24 12:12 tasks
```
il s'agit du fichier pîds.max
```
cat /sys/fs/cgroup/pids/docker/fc4afa04b5df5648586c1145c8b2428130a6284a52b9ec1a583d9dd035cf6ac4/pids.max
max
```

🌞 Altérer les valeurs cgroups allouées par défaut avec des options de la commandes `docker run` (au moins 3)
* préciser les options utilisées
* prouver en regardant dans `/sys` qu'elles sont utilisées


```
docker run -d --cpu-period=50000 --cpu-quota=25000 --cpu-shares 1048  debian sleep 99999
2eba9a9029f8370ab8499c064b8b95b29e8f4b6d58c54a9a80034d1e3b8886da
```

Pour vérifier que nos variables sont utilisées, il faut se rendre sur /sys/fs/cgroup/cpu/docker/<id_du_container>/

```
cat cpu.shares 
1048
```
```
cat cpu.cfs_quota_us 
25000
```
```
cat cpu.cfs_period_us 
50000
```

## 3. Capabilities

### A. Découverte manuelle

> 📚 [**Cours associé (sa lecture est un pré-requis).**](../../cours/containers_in_depth.md#c-capabilities)

On peut lister les capabilities utilisées par les processus du système avec la commande `pscap`.  

Il est aussi possible d'interagir avec les capabilities avec `capsh`. 

Utiliser `capsh --print`
* cela affiche des informations avancées sur votre shell
* 🌞 déterminer les capabilities actuellement utilisées par votre shell

On peut connaitre les capabilities de notre shell avec capsh
```
[felix@localhost ~]$ capsh --print
Current: =
Bounding set =cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,cap_wake_alarm,cap_block_suspend,cap_audit_read
Securebits: 00/0x0/1'b0
 secure-noroot: no (unlocked)
 secure-no-suid-fixup: no (unlocked)
 secure-keep-caps: no (unlocked)
uid=1000(felix)
gid=1000(felix)
groups=10(wheel),991(docker),1000(felix)
```
Tout les capabilities du shell sont toutes celles qui sont citées après le Bounding set :
cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,cap_wake_alarm,cap_block_suspend,cap_audit_read



---

On peut aussi obtenir des informations avec `/proc`. Pour voir les capabilities d'un processus donné :
```
$ cat /proc/<PID>/status | grep Cap

# Les valeurs obtenues sont en hexadécimal. On peut utiliser capsh pour les décoder
$ capsh --decode 0000003fffffffff
```

🌞 Déterminer les capabilities du processus lancé par un conteneur Docker
* utiliser quelque chose de simple pour le conteneur comme un `docker run -d alpine sleep 99999`
* en utilisant `/proc`
```
maxim@maxim-Predator-PH317-52:~$ docker run -d alpine sleep 99999

maxim@maxim-Predator-PH317-52:~$ docker inspect 9534269f1a08 |grep Pid
            "Pid": 9804,
            "PidMode": "",
            "PidsLimit": null,

maxim@maxim-Predator-PH317-52:~$ cat /proc/9804/status | grep Cap
CapInh:	00000000a80425fb
CapPrm:	00000000a80425fb
CapEff:	00000000a80425fb
CapBnd:	00000000a80425fb
CapAmb:	0000000000000000
```
je vais les décoder avec avec capsh
```
maxim@maxim-Predator-PH317-52:~$ capsh --decode=00000000a80425fb
0x00000000a80425fb=cap_chown,cap_dac_override,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_net_bind_service,cap_net_raw,cap_sys_chroot,cap_mknod,cap_audit_write,cap_setfcap
```
Les 4 premières cap sont les mêmes et la 5ème n'en a aucune

---

Les fichiers exécutables ont aussi un jeu de capabilities, qui définissent les capabilities qu'ils pourront acquérir une fois lancé.  

On peut voir les capabilities d'un exécutable avec `getcap <FILE>` et les définir avec `setcap <CAPABILITIES> <FILE>`.

🌞 Jouer avec `ping`
* trouver le chemin absolu de `ping`
* récupérer la liste de ses capabilities
* enlever toutes les capabilities
  * en utilisant une liste vide
  * `setcap '' <PATH>` 
* vérifier que `ping` ne fonctionne plus
* vérifier avec `strace` que c'est bien l'accès à l'ICMP qui a été enlevé
  * **NB** : vous devrez aussi ajouter des capa à `strace` pour que son `ping` puisse en hériter !

Le chemin absolu du ping est /usr/bin/ping

Pour récupérer les capabilities de ping j'utilise get cap sur le lien absolu de ping
```
maxim@maxim-Predator-PH317-52:~$ getcap /usr/bin/ping
/usr/bin/ping = cap_net_raw+ep
```
Pour enlever toutes les capabilities, on va setcap une liste vide 
```
maxim@maxim-Predator-PH317-52:~$ sudo setcap '' /usr/bin/ping
```
Je teste le ping et il ne fonctionne pas
```
maxim@maxim-Predator-PH317-52:~$ ping localhost
ping: socket: Operation not permitted
```
je lance le strace pout vérifier que c'est bien l'ICMP qui a été enlevé
```
maxim@maxim-Predator-PH317-52:~$ strace localhost
socket(AF_INET, SOCK_DGRAM, IPPROTO_ICMP) = -1 EACCES (Permission denied)
socket(AF_INET, SOCK_RAW, IPPROTO_ICMP) = -1 EPERM (Operation not permitted)
socket(AF_INET6, SOCK_DGRAM, IPPROTO_ICMPV6) = -1 EACCES (Permission denied)
socket(AF_INET6, SOCK_RAW, IPPROTO_ICMPV6) = -1 EPERM (Operation not permitted)
write(2, "ping: socket: Operation not perm"..., 38ping: socket: Operation not permitted
) = 38
```
Remettre les capabilities de ping 


maxim@maxim-Predator-PH317-52:~$ sudo setcap 'cap_net_admin,cap_net_raw+p' /usr/bin/ping

Je teste mon ping
```
maxim@maxim-Predator-PH317-52:~$ ping localhost
PING localhost(localhost (::1)) 56 data bytes
64 bytes from localhost (::1): icmp_seq=1 ttl=64 time=0.073 ms
64 bytes from localhost (::1): icmp_seq=2 ttl=64 time=0.061 ms
64 bytes from localhost (::1): icmp_seq=3 ttl=64 time=0.079 ms
^C
--- localhost ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 30ms
rtt min/avg/max/mdev = 0.061/0.071/0.079/0.007 ms
```

### B. Utilisation par Docker

Docker fait usage des capabilities nativement. Le `root` d'un conteneur est donc moins puissant que le `root` réel de la machine.  

On peut choisir les capabilities qu'un conteneur sera autorisé à utiliser avec des options de `docker run`.
* `docker run --cap-drop` pour enlever des capabilities
* `docker run --cap-add` pour ajouter des capabilities
* on peut effectuer un whitelisting des capabilities, en les enlevant toutes puis en n'autorisant uniquement celles précisées avec :  
`docker run --cap-drop all --cap-add <CAPA> --cap-add <CAPA>`  

🌞 lancer un conteneur NGINX qui a le strict nécessaire de capabilities pour fonctionner
* prouver qu'il fonctionne
* expliquer toutes les capabilities dont il a besoin
```
[felix@localhost ~]$ sudo docker run -p 80:80 --cap-drop all --cap-add=chown --cap-add=net_bind_service --cap-add=setuid --cap-add=setgid --cap-add=dac_override -d nginx
bd68935b93d94acd011d3772e8b22e084760c27e8396e8027c29bd774e12659e
```
Explication des capabilities intégrées

chown= permet de changer de proprio de fichiers
net_bind_services=permet d'exposer un port vert internet (port inférieur à 1024)
set_uid=permet de changer l'uid d'un processus
set_gid=permet de changer le gid d'un processus
dac_override=permet d'outrepasser la vérification des droits de lecture, écriture et exécution des fichiers

Il fonctionne bien en testant avec un curl
```
[felix@localhost ~]$ curl localhost:80
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```