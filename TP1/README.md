# Rendu TP cloud

## II. Lancer des conteneurs



* Lancement d'un conteneur alpine

```
docker run -d alpine sleep 9999

```
* On peut lister les conteneurs actifs avec la commande `docker ps`

```
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
f148c1e2690b        alpine              "sleep 9999"        6 seconds ago       Up 5 seconds                            affectionate_saha
```
Grâce au docker ps, on peut voir la commande "sleep 9999" dans notre conteneur

Dans notre cas, son nom est **affectionatesaha** et son id est **f148c1e2690b**.

Pour rentrer dans le conteneur à l'aide d'un shell, je tape cette commande
```
docker exec -it f148c1e2690b sh
```
une arborescence de processus différente

Sur container
```
/ # ps
PID   USER     TIME  COMMAND
    1 root      0:00 sleep 9999
    6 root      0:00 sh
   13 root      0:00 ps
```
Sur VM
```
[root@localhost docker]# ps

   PID TTY          TIME CMD
  3135 pts/0    00:00:00 sudo
  3157 pts/0    00:00:00 su
  3159 pts/0    00:00:00 bash
  4668 pts/0    00:00:00 ps
```

Les container n'ont pas de TTY


* Des utilisateurs systèmes différents


Sur container

```
/ # cat /etc/passwd
root:x:0:0:root:/root:/bin/ash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/mail:/sbin/nologin
news:x:9:13:news:/usr/lib/news:/sbin/nologin
uucp:x:10:14:uucp:/var/spool/uucppublic:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
man:x:13:15:man:/usr/man:/sbin/nologin
postmaster:x:14:12:postmaster:/var/mail:/sbin/nologin
cron:x:16:16:cron:/var/spool/cron:/sbin/nologin
ftp:x:21:21::/var/lib/ftp:/sbin/nologin
sshd:x:22:22:sshd:/dev/null:/sbin/nologin
at:x:25:25:at:/var/spool/cron/atjobs:/sbin/nologin
squid:x:31:31:Squid:/var/cache/squid:/sbin/nologin
xfs:x:33:33:X Font Server:/etc/X11/fs:/sbin/nologin
games:x:35:35:games:/usr/games:/sbin/nologin
cyrus:x:85:12::/usr/cyrus:/sbin/nologin
vpopmail:x:89:89::/var/vpopmail:/sbin/nologin
ntp:x:123:123:NTP:/var/empty:/sbin/nologin
smmsp:x:209:209:smmsp:/var/spool/mqueue:/sbin/nologin
guest:x:405:100:guest:/dev/null:/sbin/nologin
nobody:x:65534:65534:nobody:/:/sbin/nologin
```


Sur VM
```
[root@localhost docker]# cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:65534:65534:Kernel Overflow User:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
systemd-coredump:x:999:997:systemd Core Dumper:/:/sbin/nologin
systemd-resolve:x:193:193:systemd Resolver:/:/sbin/nologin
tss:x:59:59:Account used by the trousers package to sandbox the tcsd daemon:/dev/null:/sbin/nologin
polkitd:x:998:996:User for polkitd:/:/sbin/nologin
geoclue:x:997:995:User for geoclue:/var/lib/geoclue:/sbin/nologin
rtkit:x:172:172:RealtimeKit:/proc:/sbin/nologin
pulse:x:171:171:PulseAudio System Daemon:/var/run/pulse:/sbin/nologin
qemu:x:107:107:qemu user:/:/sbin/nologin
usbmuxd:x:113:113:usbmuxd user:/:/sbin/nologin
unbound:x:996:991:Unbound DNS resolver:/etc/unbound:/sbin/nologin
rpc:x:32:32:Rpcbind Daemon:/var/lib/rpcbind:/sbin/nologin
gluster:x:995:990:GlusterFS daemons:/run/gluster:/sbin/nologin
chrony:x:994:989::/var/lib/chrony:/sbin/nologin
libstoragemgmt:x:993:987:daemon account for libstoragemgmt:/var/run/lsm:/sbin/nologin
saslauth:x:992:76:Saslauthd user:/run/saslauthd:/sbin/nologin
dnsmasq:x:986:986:Dnsmasq DHCP and DNS server:/var/lib/dnsmasq:/sbin/nologin
radvd:x:75:75:radvd user:/:/sbin/nologin
clevis:x:985:984:Clevis Decryption Framework unprivileged user:/var/cache/clevis:/sbin/nologin
maxim:x:1000:1000:maxim:/home/maxim:/bin/bash
```
 
L'utilisateur maxim est présent sur ma VM mais n'est pas présent sur le conteneur

* Des cartes réseau différentes


Sur container

```
/ # ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
18: eth0@if19: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```

Sur VM

```
[root@localhost docker]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: ens33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 00:0c:29:c4:cc:27 brd ff:ff:ff:ff:ff:ff
    inet 192.168.182.129/24 brd 192.168.182.255 scope global dynamic noprefixroute ens33
       valid_lft 1114sec preferred_lft 1114sec
    inet6 fe80::9191:c240:3392:ec8d/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: ens36: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 00:0c:29:c4:cc:31 brd ff:ff:ff:ff:ff:ff
4: virbr0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default qlen 1000
    link/ether 52:54:00:ed:50:13 brd ff:ff:ff:ff:ff:ff
    inet 192.168.122.1/24 brd 192.168.122.255 scope global virbr0
       valid_lft forever preferred_lft forever
5: virbr0-nic: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel master virbr0 state DOWN group default qlen 1000
    link/ether 52:54:00:ed:50:13 brd ff:ff:ff:ff:ff:ff
6: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:71:c8:46:3d brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
```

On constate  que les cartes réseaux utilisées par le conteneurs sont différentes de celles utilisées par l'hôte. Les adresses mac et ip sont différentes.


* Des points de montage (les partitions montées) différents

```
[root@localhost docker]# df -h
Filesystem           Size  Used Avail Use% Mounted on
devtmpfs             1.4G     0  1.4G   0% /dev
tmpfs                1.4G     0  1.4G   0% /dev/shm
tmpfs                1.4G  9.8M  1.4G   1% /run
tmpfs                1.4G     0  1.4G   0% /sys/fs/cgroup
/dev/mapper/cl-root   17G  4.2G   13G  25% /
/dev/sda1            976M  184M  726M  21% /boot
tmpfs                282M   28K  282M   1% /run/user/42
tmpfs                282M  3.5M  278M   2% /run/user/1000
/dev/sr1             6.7G  6.7G     0 100% /run/media/maxim/CentOS-8-BaseOS-x86_64
overlay               17G  4.2G   13G  25% /var/lib/docker/overlay2/ce8b916816686839ce9cd7da48893be825ce432fa578e6d81bbce6bccfdf4396/merged
shm                   64M     0   64M   0% /var/lib/docker/containers/b27b27653f2d34de9bd75d285544888bd848530969d2eeb47168a3b98d1e92bb/mounts/shm
```

```
/ # df -h
Filesystem                Size      Used Available Use% Mounted on
overlay                  17.0G      4.2G     12.8G  25% /
tmpfs                    64.0M         0     64.0M   0% /dev
tmpfs                     1.4G         0      1.4G   0% /sys/fs/cgroup
/dev/mapper/cl-root      17.0G      4.2G     12.8G  25% /etc/resolv.conf
/dev/mapper/cl-root      17.0G      4.2G     12.8G  25% /etc/hostname
/dev/mapper/cl-root      17.0G      4.2G     12.8G  25% /etc/hosts
shm                      64.0M         0     64.0M   0% /dev/shm
tmpfs                     1.4G         0      1.4G   0% /proc/acpi
tmpfs                    64.0M         0     64.0M   0% /proc/kcore
tmpfs                    64.0M         0     64.0M   0% /proc/keys
tmpfs                    64.0M         0     64.0M   0% /proc/timer_list
tmpfs                    64.0M         0     64.0M   0% /proc/sched_debug
tmpfs                     1.4G         0      1.4G   0% /proc/scsi
tmpfs                     1.4G         0      1.4G   0% /sys/firmware
```


* Arrêt et suppresion du conteneur

```
docker stop affectionate_saha
docker rm affectionate_saha
```

* Lancer un container NGINX

```
docker run -d -p 80:80 nginx
```

* Arrêt de apache2 sur ma machine hôte car apache2 utilisait le port de NGINX 8080

```
systemctl stop apache2
```

La page NGINX

```
Page NGINX

Welcome to nginx!
If you see this page, the nginx web server is successfully installed and working. Further configuration is required.

For online documentation and support please refer to nginx.org.
Commercial support is available at nginx.com.

Thank you for using nginx.
```
## 2. Gestion d'images

* Récupération de l'image **apache2.2**

```
docker pull httpd:2.2
```
* Création d'images

Le Dockerfile

```
FROM alpine:latest
RUN yum install -y python3
EXPOSE 8888
WORKDIR /app
COPY test.txt test.txt
CMD python3 -m http.server 8888
```
* Partage de port sur le PC

```
docker run -d -p 3200:8888 64d2b166fa4e
```

* Partage de volume

```
docker run -v docker-python:/app 64d2b166fa4e
```

## 3. Manipulation du démon docker

* On indique a dockerd d'écouter sur le socket UNIX ainsi que le Socket tcp à l'adresse indiquée

```
[root@localhost ~]$ sudo dockerd -H unix:///var/run/docker.sock -H tcp://10.1.1.2
WARN[2020-01-06T18:39:40.303614898+01:00] [!] DON'T BIND ON ANY IP ADDRESS WITHOUT setting --tlsverify IF YOU DON'T KNOW WHAT YOU'RE DOING [!] 
INFO[2020-01-06T18:39:40.314832707+01:00] parsed scheme: "unix"                         module=grpc
INFO[2020-01-06T18:39:40.314863988+01:00] scheme "unix" not registered, fallback to default scheme  module=grpc
INFO[2020-01-06T18:39:40.314914674+01:00] parsed scheme: "unix"                         module=grpc
INFO[2020-01-06T18:39:40.314927982+01:00] scheme "unix" not registered, fallback to default scheme  module=grpc
INFO[2020-01-06T18:39:40.320038176+01:00] [graphdriver] using prior storage driver: overlay2 
INFO[2020-01-06T18:39:40.324706982+01:00] ccResolverWrapper: sending new addresses to cc: [{unix:///run/containerd/containerd.sock 0  <nil>}]  module=grpc
INFO[2020-01-06T18:39:40.324736245+01:00] ClientConn switching balancer to "pick_first"  module=grpc
INFO[2020-01-06T18:39:40.324779818+01:00] pickfirstBalancer: HandleSubConnStateChange: 0xc42015af80, CONNECTING  module=grpc
INFO[2020-01-06T18:39:40.325420154+01:00] pickfirstBalancer: HandleSubConnStateChange: 0xc42015af80, READY  module=grpc
INFO[2020-01-06T18:39:40.325448139+01:00] ccResolverWrapper: sending new addresses to cc: [{unix:///run/containerd/containerd.sock 0  <nil>}]  module=grpc
INFO[2020-01-06T18:39:40.325462319+01:00] ClientConn switching balancer to "pick_first"  module=grpc
INFO[2020-01-06T18:39:40.325492927+01:00] pickfirstBalancer: HandleSubConnStateChange: 0xc42015b210, CONNECTING  module=grpc
INFO[2020-01-06T18:39:40.325693133+01:00] pickfirstBalancer: HandleSubConnStateChange: 0xc42015b210, READY  module=grpc
INFO[2020-01-06T18:39:40.329144274+01:00] Graph migration to content-addressability took 0.00 seconds 
WARN[2020-01-06T18:39:40.329660045+01:00] Your kernel does not support cgroup blkio weight 
WARN[2020-01-06T18:39:40.329684884+01:00] Your kernel does not support cgroup blkio weight_device 
INFO[2020-01-06T18:39:40.330319974+01:00] Loading containers: start.                   
INFO[2020-01-06T18:39:40.930571835+01:00] Default bridge (docker0) is assigned with an IP address 172.17.0.0/16. Daemon option --bip can be used to set a preferred IP address 
INFO[2020-01-06T18:39:41.177634135+01:00] Loading containers: done.                    
INFO[2020-01-06T18:39:41.205554207+01:00] Docker daemon                                 commit=4c52b90 graphdriver(s)=overlay2 version=18.09.1
INFO[2020-01-06T18:39:41.205661036+01:00] Daemon has completed initialization          
INFO[2020-01-06T18:39:41.228889427+01:00] API listen on 10.1.1.2:2375                  
INFO[2020-01-06T18:39:41.229108206+01:00] API listen on /var/run/docker.sock
```

* Depuis l'hôte, on peut acceder à notre docker
```
maxim@maxim-Predator-PH317-52:~$ docker -H tcp://10.1.1.2 ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                       PORTS               NAMES
29f6d0088d62        75e62fb839d7        "/bin/sh -c 'python3…"   24 minutes ago      Exited (0) 24 minutes ago                        brave_stonebraker
b8c8be17768c        75e62fb839d7        "/bin/sh -c 'python3…"   32 minutes ago      Exited (137) 9 minutes ago                       nifty_feistel
```
* Modifier l’emplacement des données de Docker

```
service docker stop
mkdir /data/docker
vim /etc/default/docker.io
DOCKER_OPTS="-g /dockerData"
service docker start
```
* Modifier OOM 
Dans le fichier /etc/docker/daemon.json

"oom-score-adjust": -400

## II Docker-compose
docker-compose-v1.yml

```
version: "3.3"

services:
  server:
    ports:
      - "3200:8888"
    build:
     dockerfile: Dockerfile
     context: ./docker-python
```

docker-compose-v2.yml
```
version: "3.3"

services:
  nginx:
    networks:
     - local
  web:
    ports:
      - "3200:8888"
    build:
     dockerfile: Dockerfile
     context: ./docker-python
     networks:
      local:
       aliases:
           - python-app
networks:
 local:
```

